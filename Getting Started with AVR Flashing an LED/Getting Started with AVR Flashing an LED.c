/*
 * Getting_Started_with_AVR_Flashing_an_LED.c
 *
 * Created: 3/16/2015 2:05:39
 *  Author: Brandy
 */ 
#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>


int main(void)
{
	DDRB = (0 << DDB5)|(0 << DDB4)|(1 << DDB3)|(1 << DDB2)|(1 << DDB1)|(1 << DDB0);
	//Pin 3 On Port B Is Sen as an Output
    while(1)
    {
		//PORTB = (1 << PORTB3);
		PINB |= (1 << PINB3)|(1 << PINB2)|(1 << PINB1)|(1 << PINB0);
		//Toogles The PIN
		_delay_ms(5);
		//Waits for 500miliSeconds
        //TODO:: Please write your application code 
    }
}